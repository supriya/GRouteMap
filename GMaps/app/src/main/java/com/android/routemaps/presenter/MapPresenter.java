package com.android.routemaps.presenter;

import android.graphics.Color;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.android.routemaps.services.TransportServices;
import com.android.routemaps.model.DirectionsResult;
import com.android.routemaps.model.TripTrackResult;
import com.android.routemaps.utils.MapUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class MapPresenter {
    private static final String TAG = "MapPresenter";
    private interface Stencil{
        float LINE_WIDTH = 12f;
        int DIRECTIONS_LINE_COLOR = Color.RED;
        int ACTUAL_ROUTE_LINE_COLOR = Color.BLUE;
        double ACTUAL_ROUTE_CIRCLE_RADIUS = 300;
    }
    private GoogleMap mMap;

    public MapPresenter(GoogleMap googleMap) {
        mMap = googleMap;
    }

    public void init(LatLng origin, LatLng destination){
        addMarker(origin, "Pune");
        addMarker(destination, "Bangalore");
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(origin, 10));

        drawGMapsRoute(origin.latitude, origin.longitude, destination.latitude, destination.longitude);
        getAndDrawVehicleRoute();
    }

    private void getAndDrawVehicleRoute(){
        TransportServices.getInstance().getTripTrackDetails(new Callback<TripTrackResult>() {
            @Override
            public void onResponse(Call<TripTrackResult> call, Response<TripTrackResult> response) {

                if(response.isSuccessful()){
                    drawActualVehicleRoute(response.body().response.points);
                }
            }

            @Override
            public void onFailure(Call<TripTrackResult> call, Throwable t) {
            }
        });
    }

    private void drawActualVehicleRoute(List<LatLng> points){
        PolylineOptions polylineOptions = new PolylineOptions();
        setPolylineOptions(polylineOptions, Stencil.ACTUAL_ROUTE_LINE_COLOR, points);
        for (LatLng point:points){
            addCircle(point, Stencil.ACTUAL_ROUTE_LINE_COLOR);
        }
    }

    private void setPolylineOptions(PolylineOptions polylineOptions, int color, Iterable<LatLng> points){
        polylineOptions.addAll(points);
        polylineOptions.width(Stencil.LINE_WIDTH).color(color).geodesic(true);
        mMap.addPolyline(polylineOptions);
    }

    private void addCircle(LatLng latLng, int color){
        CircleOptions circleOptions = new CircleOptions()
                .center(latLng).fillColor(color).radius(Stencil.ACTUAL_ROUTE_CIRCLE_RADIUS)
                .strokeColor(color).strokeWidth(2);
        mMap.addCircle(circleOptions);
    }

    private LatLng addMarker(LatLng latlng, String title){
        mMap.addMarker(new MarkerOptions().position(latlng).title(title));
        return latlng;
    }



    private void drawGMapsRoute(double originLat, double originLng, double destLat, double destLng){
        TransportServices.getInstance().getDirections(originLat + "," + originLng, destLat + ","+destLng, new Callback<DirectionsResult>() {
            @Override
            public void onResponse(Call<DirectionsResult> call, Response<DirectionsResult> response) {
                if(response.isSuccessful()){
                    PolylineOptions polylineOptions = new PolylineOptions();
                    List<LatLng> points = new ArrayList<>();
                    for(DirectionsResult.Route route : response.body().routes){
                        for(DirectionsResult.Leg leg : route.legs){
                            for(DirectionsResult.Step step : leg.steps){
                                step.polyline.polyPoints = MapUtils.decodePoly(step.polyline.points);
                                points.addAll(step.polyline.polyPoints);
                            }
                        }
                    }
                    setPolylineOptions(polylineOptions, Stencil.DIRECTIONS_LINE_COLOR, points);
                }
            }

            @Override
            public void onFailure(Call<DirectionsResult> call, Throwable t) {

            }
        });
    }
}
