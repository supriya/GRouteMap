package com.android.routemaps.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;



public class TripTrackResult {
    public Response response;

    public static class Response{
        public transient List<LatLng> points;

        public static class Deserializer implements JsonDeserializer<Response>{

            @Override
            public Response deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                JsonObject object = json.getAsJsonObject();
                JsonArray array = object.getAsJsonArray("vehicleTracks");
                Response response = new Response();
                response.points = new ArrayList<>();
                for (JsonElement el:array
                        ) {
                    JsonObject obj = el.getAsJsonObject();
                    LatLng point = new LatLng(getDouble(obj, "latitude"), getDouble(obj, "longitude"));
                    response.points.add(point);
                }
                return response;
            }

            private double getDouble(JsonObject obj, String key){
                return Double.parseDouble(obj.get(key).getAsString());
            }
        }
    }
}
