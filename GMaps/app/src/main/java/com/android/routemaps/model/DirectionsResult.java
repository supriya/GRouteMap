package com.android.routemaps.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;


public class DirectionsResult {
    public List<Route> routes;

    public static class Route {
        public List<Leg> legs;
    }

    public static class Leg {
        public List<Step> steps;
    }

    public static class Step {
        public Polyline polyline;
    }

    public static class Polyline {
        public String points;
        public List<LatLng> polyPoints;
    }
}
