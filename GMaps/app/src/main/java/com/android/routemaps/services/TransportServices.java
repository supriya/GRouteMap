package com.android.routemaps.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.android.routemaps.model.DirectionsResult;
import com.android.routemaps.model.TripTrackResult;
import com.android.routemaps.utils.AppConstants;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;


public class TransportServices {
    private static TransportServices sInstance;
    private GMapsService mGMapsService;
    private BackendService mBackendService;

    public interface GMapsService{
        @GET("directions/json")
        Call<DirectionsResult> getDirections(@Query("origin") String originLtLng, @Query("destination") String destinationLtLng, @Query("key") String apiKey);
    }

    public interface BackendService{
        @GET("gettriplist.json?dl=0")
        Call<TripTrackResult> getTripTrackDetails();
    }

    public static synchronized TransportServices getInstance(){
        if(sInstance == null){
            sInstance = new TransportServices();
        }
        return sInstance;
    }

    private TransportServices(){
        build();
    }

    private void build(){
        buildGMapsService();
        buildBackendService();
    }

    private void buildBackendService() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(TripTrackResult.Response.class, new TripTrackResult.Response.Deserializer()).create();
        Retrofit.Builder retroBuilder = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson));

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        retroBuilder.client(httpClient.build());

        Retrofit retro = retroBuilder
                .build();
        mBackendService = retro.create(BackendService.class);
    }

    private void buildGMapsService() {
        Retrofit retro = new Retrofit.Builder()
                .baseUrl(AppConstants.GOOGLE_MAP_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mGMapsService = retro.create(GMapsService.class);
    }

    public void getDirections(String originLtLng, String destinationLtLng, Callback<DirectionsResult> callback){
        Call<DirectionsResult> call = mGMapsService.getDirections(originLtLng, destinationLtLng, "keep-ur-key");
        call.enqueue(callback);
    }

    public void getTripTrackDetails(Callback<TripTrackResult> callback){
        Call<TripTrackResult> call = mBackendService.getTripTrackDetails();
        call.enqueue(callback);
    }

}
