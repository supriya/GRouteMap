package com.android.routemaps.view;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.android.routemaps.R;
import com.android.routemaps.presenter.MapPresenter;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapPresenter presenter = new MapPresenter(googleMap);

        //hardcoded lat lng for testing purpose
        double originLat = 18.5563296, originLng = 73.8741167,
                destLat = 12.9542946, destLng = 77.4908521;

        presenter.init(new LatLng(originLat, originLng), new LatLng(destLat, destLng));
    }
}
